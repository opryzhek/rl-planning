import cv2
import numpy as np
from puzzlegen import IceSlider, DigitJump


def get_input_shape(env_name, encode_position, **kwargs):
    """
    Returns the input shape of the encoder for particular environments.
    """
    shape_dict = {
        'ice_slider': (3, 64, 64),
        'digit_jump': (3, 64, 64),
    }

    shape = shape_dict[env_name]
    if "rescale" in kwargs["env_params"] and not kwargs["env_params"]["rescale"]:
        shape = (3, 8, 8)
    return (shape[0]+2,) + shape[1:] if encode_position else shape


def get_no_op(env_name):
    """
    Returns the no-op key for a particular environment.
    """
    no_opt_dict = {'ice_slider': 4, 'digit_jump': 4}
    return no_opt_dict[env_name]


def get_model_to_env_action_dict(env_name):
    """
    Returns the map from action keys used in the model (sequential integers starting from 0) to action keys used in the
    environment.
    """
    d = {
        'ice_slider': {0: 0, 1: 1, 2: 2, 3: 3, 4: 4},
        'digit_jump': {0: 0, 1: 1, 2: 2, 3: 3, 4: 4},

    }
    return d[env_name]


def get_env_to_model_action_dict(env_name):
    """
    Returns the map from action keys used in the environment to action keys used in the model (sequential integers
    starting from 0).
    """
    return {v: k for k, v in get_model_to_env_action_dict(env_name).items()}


def get_model_action_set(env_name):
    """
    Returns all action keys that can be given as input to the model.
    """
    return [k for k, v in get_model_to_env_action_dict(env_name).items()]


def get_action_space_size(env_name):
    return len(get_model_action_set(env_name))


def get_env_action_set(env_name):
    """
    Returns all action keys the environment accepts.
    """
    return [v for k, v in get_model_to_env_action_dict(env_name).items()]


def env_to_model_action_map(a, env_name):
    """
    Maps from procgen action space to the restricted action space used by the model.
    """
    original_shape = a.shape
    flat_a = a.copy().reshape(-1)
    d = get_env_to_model_action_dict(env_name)
    for i in range(len(flat_a)):
        flat_a[i] = d[flat_a[i]]
    return flat_a.reshape(original_shape)


def model_to_env_action_map(a, env_name):
    """
    Maps from the restricted model action space to procgen action space.
    """
    original_shape = a.shape
    flat_a = a.copy().reshape(-1)
    d = get_model_to_env_action_dict(env_name)
    for i in range(len(flat_a)):
        flat_a[i] = d[flat_a[i]]
    return flat_a.reshape(original_shape)


def represent_same_state(a, b, env_name, encode_position, **kwargs):
    if env_name == 'maze':
        return True if np.count_nonzero(np.count_nonzero(a - b, axis=0)) <= 1 else False
    if env_name in ['digit_jump', 'ice_slider']:
        return np.allclose(a, b)
    raise NotImplementedError('Unknown environment.')


def make_env(env_name, seed, env_params=None):
    puzzle_fn = {'ice_slider': GridIceSlider, 'digit_jump': DigitJump}
    if env_name in ['ice_slider', 'digit_jump']:
        return puzzle_fn[env_name](seed=seed, **({} if env_params is None else env_params))
    raise Exception('Unknown environment.')


class GridIceSlider(IceSlider):
    def __init__(self, ice_density=4, easy=False, render_style='human', min_sol_len=8, rescale=True, **kwargs):
        super().__init__(ice_density, easy, render_style, min_sol_len, **kwargs)
        self.rescale = rescale

    def _get_image(self):
        if self.render_style == "human":
            return super()._get_image()
        else:
            rgb = np.concatenate([np.concatenate([self.rock_rgb if not el else (
                self.player_rgb if self.pos == (i, j) else (self.goal_rgb if self.end == (i, j) else self.ice_rgb))
                                                  for j, el in enumerate(row)], axis=1) for i, row in
                                  enumerate(self.grid)], axis=0)

            if self.rescale:
                rescaled = cv2.resize(rgb, (64, 64), interpolation=cv2.INTER_NEAREST)
            else:
                rescaled = rgb
            render = np.clip(rescaled, 0, 255)
            return render.astype(np.uint8)